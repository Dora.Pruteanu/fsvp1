package edu.princeton.cs.algs4;

import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import edu.princeton.cs.algs4.beyond.FibonacciMinPQ;
import edu.princeton.cs.algs4.collections.Vector;
import net.jqwik.api.Assume;
import net.jqwik.api.ForAll;

import net.jqwik.api.Property;
import net.jqwik.api.constraints.*;

import java.util.Arrays;
import java.util.Comparator;


class VectorTest{
	
	
     @Property
     void testEmptyConstructor(@ForAll @IntRange(min=0, max=2000) int d) {
    	 Vector vec = new Vector(d);
    	 Assertions.assertEquals(vec.dimension(), d);
     }
     
     @Property
     void testEmptyConstructorThrowsOnNegativeDimension(@ForAll @IntRange(min=-2000, max=-1) int d) {
    	 Assertions.assertThrows(NegativeArraySizeException.class, () -> new Vector(d));
     }
     
     @Property
     void testPrefilledConstructor(@ForAll @Size(min=0, max=300) double[] v) {
    	 Vector vec = new Vector(v);
    	 Assertions.assertEquals(vec.dimension(), v.length);
     }
     
     @Property
     void testDimensionWithPositiveNumber(@ForAll @IntRange(min=0, max=2000) int d, @ForAll int e, @ForAll int f, @ForAll int g) {
    	 Vector vec = new Vector(d);
    	 Assertions.assertEquals(vec.dimension(), d);
    	 
    	 Vector vec2 = new Vector(d,e,f,g);
    	 double[] array = {d,e,f,g};
    	 Assertions.assertEquals(array.length, vec2.dimension());
     }
   
     
     @Property
     void testDotDifferentDimensions(@ForAll @Size(min=0, max=300) double[] v,@ForAll @Size(min=0, max=300) double[] n) {
    	 Assume.that(n.length > v.length);
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    	 Assertions.assertThrows(IllegalArgumentException.class, () -> vec.dot(vec2));
    	 
    	 
     }
     @Property
     void testDot(@ForAll @Size(100) double[] v,@ForAll @Size(100) double[] n) {
    	 Assume.that(n.length == v.length);
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    
    	 Assertions.assertEquals(scalarproduct(vec,vec2), vec.dot(vec2));
    	 
    	 
     }
     @Property
     void testMagnitude(@ForAll double[] v) {
    	 Vector vec = new Vector(v);
    	 Assertions.assertEquals(Math.sqrt(scalarproduct(vec,vec)), vec.magnitude());
     }
     
     @Property
     void testMagnitudeWithBothZeroVector(@ForAll @IntRange( min = 0,  max = 10) int v) {
    	 Vector vec = new Vector(v);
    	 Assertions.assertEquals(Math.sqrt(scalarproduct(vec,vec)), vec.magnitude());
     }
     
     @Property
     void testMinusWithDifferentDimensions(@ForAll @Size(min=0, max=300) double[] v,@ForAll @Size(min=0, max=300) double[] n) {
    	 Assume.that(n.length > v.length);
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    	 
    	 Assertions.assertThrows(IllegalArgumentException.class, () -> vec.minus(vec2));
     }
     
     @Property
     void testMinusWithSameDimensions(@ForAll @Size(10) double[] v,@ForAll @Size(10) double[] n) {
    	
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    	 for(int i = 0; i < vec.dimension(); i++) {
    	 Assertions.assertEquals(testMinus(v,n).cartesian(i), vec.minus(vec2).cartesian(i));}
     }
     
     @Property
     void testMinusWithZeroVector(@ForAll @IntRange( min = 0,  max = 10) int d) {
    	 Vector vec = new Vector(d);
    	 double[] array = new double[d];
    	 for(int i = 0; i < vec.dimension(); i++) {
        	 Assertions.assertEquals(testMinus(array,array).cartesian(i), vec.minus(vec).cartesian(i));}
     }
     @Property
     void testPlusWithDifferentDimensions(@ForAll @Size(min=0, max=300) double[] v,@ForAll @Size(min=0, max=300) double[] n) {
    	 Assume.that(n.length > v.length);
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    	 
    	 Assertions.assertThrows(IllegalArgumentException.class, () -> vec.plus(vec2));
     }
     
     @Property
     void testPlusWithSameDimensions(@ForAll @Size(9) double[] v,@ForAll @Size(9) double[] n) {
    	
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    	 for(int i = 0; i < vec.dimension(); i++) {
    	 Assertions.assertEquals(testPlus(v,n).cartesian(i), vec.plus(vec2).cartesian(i));}
     }
     
     @Property
     void testPlusWithZeroVector(@ForAll @IntRange( min = 0,  max = 10) int d) {
    	 Vector vec = new Vector(d);
    	 double[] array = new double[d];
    	 for(int i = 0; i < vec.dimension(); i++) {
        	 Assertions.assertEquals(testPlus(array,array).cartesian(i), vec.plus(vec).cartesian(i));}
     }
     
     @Property
     void testDistanceToWithDifferentDimensions(@ForAll @Size(min=0, max=300) double[] v,@ForAll @Size(min=0, max=300) double[] n) {
    	 Assume.that(n.length > v.length);
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    	 
    	 Assertions.assertThrows(IllegalArgumentException.class, () -> vec.distanceTo(vec2));
     }
     @Property
     void testDistanceToWithSameDimensions(@ForAll @Size(11) double[] v,@ForAll @Size(11) double[] n) {
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    	 Assertions.assertEquals(Math.sqrt(scalarproduct(testMinus(v,n),testMinus(v,n))), vec.distanceTo(vec2));
    	 
     }
     @Property
     void testDistanceToWithZeroVector(@ForAll @IntRange( min = 0,  max = 10) int d) {
    	 Vector vec = new Vector(d);
    	 double[] array = new double[d];
    	 Assertions.assertEquals(Math.sqrt(scalarproduct(testMinus(array,array),testMinus(array,array))), vec.distanceTo(vec));
     }
     @Property
     void testDistanceToWithSameVector(@ForAll @Size(11) double[] v) {
    	 Vector vec = new Vector(v);
    	 
    	 Assertions.assertEquals(Math.sqrt(scalarproduct(testMinus(v,v),testMinus(v,v))), vec.distanceTo(vec));
     }
     @Property
     void testScale(@ForAll @Size(11) double[] v, @ForAll double alpha, @ForAll @IntRange( min = 0,  max = 10) int n) {
    	 Vector vec = new Vector(v);
    	 Vector vec2 = new Vector(n);
    	 double[] nArray= new double[n];
    	 int alphaZero=0;
    	 for(int i=0; i<vec.dimension();i++) {
    	 Assertions.assertEquals(testScale(alpha,v).cartesian(i), vec.scale(alpha).cartesian(i));
    	 Assertions.assertEquals(testScale(alphaZero,v).cartesian(i), vec.scale(alphaZero).cartesian(i));
    	 }
    	 for(int i=0; i<vec2.dimension();i++) {
    		 Assertions.assertEquals(testScale(alpha,nArray).cartesian(i), vec2.scale(alpha).cartesian(i));
    		 Assertions.assertEquals(testScale(alphaZero,nArray).cartesian(i), vec2.scale(alphaZero).cartesian(i));
    	 }
    	 
    	 
    	 
     }
     
     @Property
     void testDirectionZeroVector(@ForAll @Size(min=5, max=30)  @DoubleRange(min = 0, max = 0) double[] n) {
    	 Vector vec = new Vector(n); 
    	 Assertions.assertThrows(ArithmeticException.class, () -> vec.direction()) ;
     }
     
     @Property
     void testDirection(@ForAll @Size(min=1, max=5) @DoubleRange(min = -2000, max =2000) double[] n) {
    	 Vector vec = new Vector(n);  	 
    	 Assume.that(vec.magnitude() != 0);
    	 
    	 double unitMag = vec.direction().magnitude();
    	 
    	 Assertions.assertEquals(unitMag,1.0, 0.00001);
     }
     
     @Property
     void testToString(@ForAll @Size(min=1, max=5) @DoubleRange(min = -2000, max =2000) double[] n) {
    	 Vector vec = new Vector(n);
    	 
    	 String res = vec.toString();
    	 Assertions.assertTrue(res.length() > 0);
    	 Assertions.assertEquals(res.split("\s").length, n.length); 
     }
     
     @Property
     void testCartesian(@ForAll @Size(min=1, max=200) @DoubleRange(min = -2000, max =2000) double[] n) {
    	 Vector vec = new Vector(n);
    	 
    	 for(int i = 0; i < n.length; i++) {
    		 Assertions.assertEquals(n[i], vec.cartesian(i));
    	 }
    	 
     }
     
     private double scalarproduct(Vector q, Vector vec){

    	    double scalarproduct = 0.0;
    	  
    	    if (q.dimension() == vec.dimension()){

    	        for(int i = 0; i < q.dimension(); i++){
    	            scalarproduct = scalarproduct + q.cartesian(i) * vec.cartesian(i);
    	        }
    	    }

    	    return scalarproduct;
    	}
     private Vector testMinus(double[] q, double[] vec) {
    	 
    		 double[] array = new  double[vec.length]; 
    		 for(int i = 0; i < q.length; i++) {
    			  array[i] = q[i] - vec[i];
    		 }
    		 Vector result = new Vector(array);
    		 return result;
    	 
    	 
     }
     private Vector testPlus(double[] q, double[] vec) {
    	 
    		 double[] array = new  double[vec.length]; 
    		 for(int i = 0; i < q.length; i++) {
    			  array[i] = q[i] + vec[i];
    		 }
    		 Vector result = new Vector(array);
    		 return result;
    	 
    	 
     }
     private Vector testScale(double alpha,double[] array) {
        
         double[] resultArray = new double[array.length];
         for (int i = 0; i < array.length; i++)
             resultArray[i] = alpha * array[i];
         Vector result = new Vector(resultArray);
         return result;
     }
     
}