package edu.princeton.cs.algs4;

import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import edu.princeton.cs.algs4.beyond.FibonacciMinPQ;
import net.jqwik.api.Assume;
import net.jqwik.api.ForAll;
import net.jqwik.api.Property;
import net.jqwik.api.constraints.Positive;

import java.util.Arrays;
import java.util.Comparator;


class FibonacciMinPQTest{
	
	/**@Before
	void setUp() {
		Comparator<Integer> C ;
		Integer[] intArray  =  {4,8,2,0,9,1233,7346,238,-332,9393,74672,-55,-0};
		FibonacciMinPQ<Integer> fibIntEmpty = new FibonacciMinPQ<Integer>();
		FibonacciMinPQ<Integer> fibIntComp = new FibonacciMinPQ<Integer>(C);
		FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(intArray);
		FibonacciMinPQ<Integer> fibIntEmpty = new FibonacciMinPQ<Integer>();
		
	}**/
	/*@Test
	void testInitialise(@ForAll List<Integer> list, @ForAll Comparator<Integer> C ) {
		Integer[] intArray = (Integer[]) list.toArray();
		
		
		FibonacciMinPQ<Integer> fibIntEmpty = new FibonacciMinPQ<Integer>();
		FibonacciMinPQ<Integer> fibIntComp = new FibonacciMinPQ<Integer>(C);
		FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(intArray);
		FibonacciMinPQ<Integer> fibIntBoth = new FibonacciMinPQ<Integer>(C,intArray);
		
	}*/

    @Property
    void testForEmpty(@ForAll int n){
    	FibonacciMinPQ<Integer> fibIntEmpty = new FibonacciMinPQ<Integer>();
    			Assertions.assertEquals(true
    					, fibIntEmpty.isEmpty());;
    	fibIntEmpty.insert(n);
    	
    	Assertions.assertEquals(false
				, fibIntEmpty.isEmpty());;	


    }
    @Property
    void testMinKeyNotEmpty(@ForAll int n, @ForAll int m, @ForAll int g, @ForAll int b) {
    	
    	Integer[] array = {n,m,g,b};
    	
    	FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(array);
    	Arrays.sort(array);
    	Assertions.assertEquals(array[0], fibIntArray.minKey());
    	
    	
    }
    @Property
    void testMinKeyEmpty() {
    	
    	FibonacciMinPQ<String> fib = new FibonacciMinPQ<String>();
    	 Throwable exception = Assertions.assertThrows(NoSuchElementException.class, () -> fib.minKey());
    	    Assertions.assertEquals("Priority queue is empty", exception.getMessage());
    	
    }
    @Property
    void testSizeNotEmpty(@ForAll int n) {
    	Comparator<Integer> C = Comparator.comparingInt(a -> a);
    	Integer[] array = {n};
    	FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(C,array);
    	int arraySize= array.length;
    	Assertions.assertEquals(arraySize, fibIntArray.size());
    }
    
    @Property
    void testSizeEmpty() {
    	Comparator<Integer> C = Comparator.comparingInt(a -> a);
    	
    	FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(C);
    	Assertions.assertEquals(0, fibIntArray.size());
    }
    @Property
    void testInsertIntoEmpty(@ForAll int n) {
    	FibonacciMinPQ<Integer> fib = new FibonacciMinPQ<Integer>();
    	fib.insert(n);
    	Assertions.assertEquals(n, fib.minKey());
    	Assertions.assertEquals(1, fib.size());
    }
    @Property
    void testInsertNotEmptyMinFirst(@ForAll @Positive int n, @ForAll @Positive int p) {
    	Assume.that(n > p);
    	FibonacciMinPQ<Integer> fib = new FibonacciMinPQ<Integer>();
    	fib.insert(p);
    	fib.insert(n);
    	Assertions.assertEquals(p, fib.minKey());
    	Assertions.assertEquals(2, fib.size());
    	
    }
    @Property
    void testInsertNotEmptyMinSecond(@ForAll  int n, @ForAll  int p) {
    	Assume.that(n > p);
    	FibonacciMinPQ<Integer> fib = new FibonacciMinPQ<Integer>();
    	fib.insert(n);
    	fib.insert(p);
    	Assertions.assertEquals(p, fib.minKey());
    	Assertions.assertEquals(2, fib.size());
    	
    }
    @Property
    void testInsertNull() {
    	FibonacciMinPQ<Integer> fib = new FibonacciMinPQ<Integer>();
    	Integer n =null;
    	
    	 Throwable exception = Assertions.assertThrows(NullPointerException.class, () -> fib.insert(n));
 	    Assertions.assertEquals("The key is null", exception.getMessage());
    	
    }
     @Property
     void testDelMinOnEmpty() {
    	 FibonacciMinPQ<Integer> fib = new FibonacciMinPQ<Integer>();
    	 Throwable exception = Assertions.assertThrows(NoSuchElementException.class, () -> fib.delMin());
  	    Assertions.assertEquals("Priority queue is empty", exception.getMessage());
    	 
     }
     @Property
     void testDelMinWithElements(@ForAll  int n,@ForAll int a,@ForAll int b) {
    	 Integer[] array = {n,a,b};
     	FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(array);
     	
     	fibIntArray.delMin();
     	
     	Arrays.sort(array);
     	
     	Assertions.assertEquals(array[1],fibIntArray.minKey() );
     	Assertions.assertEquals(array.length-1, fibIntArray.size());
     }
     @Property
     void testDelMinOnOnenElement(@ForAll  int n) {
    	 FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>();
    	 fibIntArray.insert(n);
    	 fibIntArray.delMin();
    	 Assertions.assertEquals(true, fibIntArray.isEmpty());
    	 
     }
     @Property
     void testDelMinTriggerMeld(@ForAll  int n,@ForAll int a,@ForAll int b,@ForAll int c,@ForAll int d,@ForAll int e) {
    	 Integer[] array = {n,a,b,e,d,c};
    	 Comparator<Integer> C = Comparator.comparingInt(g -> g);
    	 FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(C,array);
    	 fibIntArray.iterator();
    	 fibIntArray.delMin();
    	 Arrays.sort(array);
    	 fibIntArray.delMin();
      	
      	Assertions.assertEquals(array[2],fibIntArray.minKey() );
    	 
     }
     @Property
     void testUnionWithElementsInBothArrays(@ForAll  int n,@ForAll int a,@ForAll int b,@ForAll int c,@ForAll int d,@ForAll int e) {
    	 Integer[] array = {n,a,b};
    	 Integer[] secondArray = {c,d,e};
    	 
    	 FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(array);
    	 FibonacciMinPQ<Integer> fibIntArraySecond = new FibonacciMinPQ<Integer>(secondArray);
    	 
    	 Arrays.sort(array);
    	 Arrays.sort(secondArray);
    	 Integer minValue;
    	 if(array[0]<secondArray[0]) {
    		 minValue=array[0];
    	 }else {
    		 minValue=secondArray[0];
    	 }
    	 fibIntArray.union(fibIntArraySecond);
    	 Assertions.assertEquals(minValue, fibIntArray.minKey());
    	 Assertions.assertEquals(array.length +secondArray.length, fibIntArray.size());
     }
     @Property
     void testUnionWithElemntsInOneArrayAndBothEmpty(@ForAll  int n,@ForAll int a,@ForAll int b,@ForAll int c,@ForAll int d,@ForAll int e) {
    	 Integer[] array = {n,a,b,c,d,e};
    	 Integer[] arrayEmpty= {};
    	 
    	 FibonacciMinPQ<Integer> fibIntArray = new FibonacciMinPQ<Integer>(array);
    	 FibonacciMinPQ<Integer> fibIntArraySecond = new FibonacciMinPQ<Integer>(arrayEmpty);
    	 
    	 
    	 Throwable exception = Assertions.assertThrows(NoSuchElementException.class, () -> fibIntArray.union(fibIntArraySecond));
   	    Assertions.assertEquals("Priority queue is empty", exception.getMessage());
   	 Throwable exceptionSecond = Assertions.assertThrows(NoSuchElementException.class, () -> fibIntArraySecond.union(fibIntArray));
	    Assertions.assertEquals("Priority queue is empty", exceptionSecond.getMessage());   
	    
	    FibonacciMinPQ<Integer> fibEmpty = new FibonacciMinPQ<Integer>();
   	 FibonacciMinPQ<Integer> fibEmptySecond = new FibonacciMinPQ<Integer>();
   	Throwable exceptionThird = Assertions.assertThrows(NoSuchElementException.class, () -> fibEmptySecond.union(fibEmpty));
    Assertions.assertEquals("Priority queue is empty", exceptionThird.getMessage()); 
     }
     
     
     
}