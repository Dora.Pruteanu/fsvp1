package edu.princeton.cs.algs4;

import java.util.Iterator;
import java.util.List;
import java.util.HashMap;
import java.util.NoSuchElementException;

import org.junit.Before;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import edu.princeton.cs.algs4.sorting.*;
import net.jqwik.api.Assume;
import net.jqwik.api.ForAll;

import net.jqwik.api.Property;
import net.jqwik.api.constraints.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;


class MergeTest{
	
	
    @Property
    void testSortInteger(@ForAll Integer[] source){
    	Comparable<Integer>[] toSort = source;
    	Comparable<Integer>[] manualToSort = source;
    	Merge.sort(toSort);
    	Arrays.sort(manualToSort);
    	Assertions.assertArrayEquals(toSort, manualToSort);
    }
    
    @Property
    void testSortString(@ForAll String[] source){
    	Comparable<String>[] toSort = source;
    	Comparable<String>[] manualToSort = source;
    	Merge.sort(toSort);
    	Arrays.sort(manualToSort);
    	Assertions.assertArrayEquals(toSort, manualToSort);
    }
    
    @Property
    void testIndexSortInteger(@ForAll Integer[] source){
    	Comparable<Integer>[] toSort = source;
    	
    	int[] indexSort =  Merge.indexSort(toSort);
    	
    	
    	
    	
    	ArrayList<Comparable<Integer>> resultList = new ArrayList<Comparable<Integer>>();

    	for(int el : indexSort) {
    		resultList.add(toSort[el]);
  	    }

    	Comparable<Integer>[] manualToSort = source;
    	Arrays.sort(manualToSort);
    	
    	for(int i = 0; i < resultList.size(); i++) {
    		Assertions.assertEquals(resultList.get(i), manualToSort[i]);
    	}
    }
    @Property
    void testIndexSortString(@ForAll String[] source){
    	Comparable<String>[] toSort = source;
    	
    	int[] indexSort =  Merge.indexSort(toSort);
    	
    	
    	
    	
    	ArrayList<Comparable<String>> resultList = new ArrayList<Comparable<String>>();

    	for(int el : indexSort) {
    		resultList.add(toSort[el]);
  	    }

    	Comparable<String>[] manualToSort = source;
    	Arrays.sort(manualToSort);
    	
    	for(int i = 0; i < resultList.size(); i++) {
    		Assertions.assertEquals(resultList.get(i), manualToSort[i]);
    	}
    }
}